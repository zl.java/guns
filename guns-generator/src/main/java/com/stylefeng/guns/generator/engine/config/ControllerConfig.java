package com.stylefeng.guns.generator.engine.config;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 控制器模板生成的配置
 *
 * @author fengshuonan
 * @date 2017-05-07 22:12
 */
@Data
public class ControllerConfig {

	private ContextConfig contextConfig;

	private String controllerPathTemplate;

	private String packageName;//包名称

	private List<String> imports;//所引入的包

	public void init() {
		ArrayList<String> imports = new ArrayList<>();
		imports.add(contextConfig.getCoreBasePackage() + ".base.controller.BaseController");
		imports.add("io.swagger.annotations.Api");
		imports.add("io.swagger.annotations.ApiOperation");
		imports.add("org.springframework.stereotype.Controller");
		imports.add("org.springframework.web.bind.annotation.PathVariable");
		imports.add("org.springframework.web.bind.annotation.RequestParam");
		imports.add("org.springframework.web.bind.annotation.RequestMapping");
		imports.add("org.springframework.web.bind.annotation.ResponseBody");
		imports.add("org.springframework.web.bind.annotation.RequestMethod");
		imports.add("org.springframework.ui.Model");
		imports.add("org.springframework.beans.factory.annotation.Autowired");
		imports.add(contextConfig.getProPackage() + ".core.log.LogObjectHolder");
		imports.add(contextConfig.getModelPackageName() + "." + contextConfig.getEntityName());
		imports.add(contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".service" + ".I" + contextConfig.getEntityName() + "Service");
		this.imports = imports;
		this.packageName = contextConfig.getProPackage() + ".modular." + contextConfig.getModuleName() + ".controller";
		this.controllerPathTemplate = "\\src\\main\\java\\" + contextConfig.getProPackage().replaceAll("\\.", "\\\\") + "\\modular\\" + contextConfig.getModuleName() + "\\controller\\{}Controller.java";
	}

}
