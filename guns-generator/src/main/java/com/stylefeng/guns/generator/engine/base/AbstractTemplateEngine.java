package com.stylefeng.guns.generator.engine.base;

import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.stylefeng.guns.generator.engine.config.*;
import lombok.Data;

/**
 * 模板生成父类
 *
 * @author fengshuonan
 * @date 2017-05-08 20:17
 */
@Data
public class AbstractTemplateEngine {

	protected ContextConfig contextConfig;                //全局配置

	protected ControllerConfig controllerConfig;          //控制器的配置

	protected PageConfig pageConfig;                      //页面的控制器

	protected DaoConfig daoConfig;                        //Dao配置

	protected ServiceConfig serviceConfig;                //Service配置

	protected SqlConfig sqlConfig;                        //sql配置

	protected TableInfo tableInfo;                        //表的信息

	public void initConfig() {
		if (this.contextConfig == null) {
			this.contextConfig = new ContextConfig();
		}
		if (this.controllerConfig == null) {
			this.controllerConfig = new ControllerConfig();
		}
		if (this.pageConfig == null) {
			this.pageConfig = new PageConfig();
		}
		if (this.daoConfig == null) {
			this.daoConfig = new DaoConfig();
		}
		if (this.serviceConfig == null) {
			this.serviceConfig = new ServiceConfig();
		}
		if (this.sqlConfig == null) {
			this.sqlConfig = new SqlConfig();
		}
		this.contextConfig.init();

		this.controllerConfig.setContextConfig(this.contextConfig);
		this.controllerConfig.init();

		this.serviceConfig.setContextConfig(this.contextConfig);
		this.serviceConfig.init();

		this.daoConfig.setContextConfig(this.contextConfig);
		this.daoConfig.init();

		this.pageConfig.setContextConfig(this.contextConfig);
		this.pageConfig.init();

		this.sqlConfig.setContextConfig(this.contextConfig);
		this.sqlConfig.init();
	}

}

